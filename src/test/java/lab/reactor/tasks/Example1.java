package lab.reactor.tasks;

import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.GroupedFlux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuples;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.Function;

@Slf4j
public class Example1 {

    @Builder
    @Data
    static class Details {
        Integer id;
        String desc;
        Status status;
    }
    enum Status {
        UP, DOWN, NONE
    }
    Map<Integer, Details> detailsMap = Map.of(
            1, Details.builder().id(1).desc("jeden").status(Status.UP).build(),
            2, Details.builder().id(2).desc("dwa").status(Status.DOWN).build(),
            3, Details.builder().id(3).desc("trzy").status(Status.UP).build(),
            4, Details.builder().id(4).desc("cztery").status(Status.DOWN).build(),
            5, Details.builder().id(5).desc("pięć").status(Status.UP).build(),
            6, Details.builder().id(6).desc("sześć").status(Status.DOWN).build(),
            7, Details.builder().id(7).desc("siedem").status(Status.UP).build(),
            8, Details.builder().id(8).desc("osiem").status(Status.DOWN).build(),
            9, Details.builder().id(9).desc("dziewięć").status(Status.UP).build(),
            10, Details.builder().id(10).desc("dziesięć").status(Status.NONE).build()
    );
    Mono<Details> getDetails(Integer key) {
        return Mono.just(detailsMap.get(key));
    }
    Mono<List<Integer>> getIds() {
        return Mono.fromCallable(()->Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
    }
    @Test
    // wyjscie z listy identyfikatorów, dociaganie szczególów,  grupowanie po status i
    // policzenie ile jest w kazdej z grupy
    public void test() {
        Mono<Map<Status, Mono<AtomicInteger>>> map =
                getIds()
                .flatMapIterable(Function.identity())
                .flatMap(this::getDetails, 2)
                .groupBy(Details::getStatus, Function.identity())
                .collectMap(GroupedFlux::key,
                        g -> g.reduce(new AtomicInteger(), (ai, details) -> {
                            log.info("status: {}, quantity: {} ", details.status, ai.incrementAndGet());
                            return ai;
                        }));
        map.subscribe(f -> log.info("map: {}", f));
    }

    @Test
    public void solveMichal1() {
        Flux<Status> statusFlux = getIds()
                .flatMapIterable(Function.identity())
                .flatMap(this::getDetails, 2)
                .map(Details::getStatus)
                .cache();

        Mono<Map<Status, Long>> statusCountMap = statusFlux.distinct()
                .flatMap(s -> statusFlux
                        .filter(s1 -> s1.equals(s))
                        .count()
                        .map(v -> Pair.of(s, v)))
                .collectMap(p -> p.getLeft(), p -> p.getRight());

        statusCountMap.subscribe(m -> log.info("map: {}", m));
    }

    @Test
    void solveMichal2() {
        Mono<Map<Status, Mono<AtomicInteger>>> weirdStatusCountMap = getIds()
                .flatMapIterable(Function.identity())
                .flatMap(this::getDetails)
                .groupBy(Details::getStatus, Function.identity())
                .collectMap(GroupedFlux::key,
                        g -> g.reduce(new AtomicInteger(), (t, v) -> {
                            log.info("status: {}, quantity: {} ", v.status, t.incrementAndGet());
                            return t;
                        }));

        Mono<Map<Status, AtomicInteger>> statusCountMap = weirdStatusCountMap.flatMapIterable(m -> m.keySet())
                .flatMap(s -> weirdStatusCountMap.flatMap(m1 -> m1.get(s))
                        .map(i -> Pair.of(s, i))
                ).collectMap(p -> p.getLeft(), p -> p.getRight());

        statusCountMap.subscribe(m -> log.info("map: {}", m));
    }

    @Test
    void solveMichal3() {
        Mono<Map<Status, AtomicInteger>> statusCountMap = getIds()
                .flatMapIterable(Function.identity())
                .flatMap(this::getDetails, 2)
                .map(Details::getStatus)
                .reduce(new ConcurrentHashMap<>(), createAccumulatorToMap());

        statusCountMap.subscribe(m -> log.info("map: {}", m));
    }

    private BiFunction<Map<Status, AtomicInteger>, Status, Map<Status, AtomicInteger>> createAccumulatorToMap() {
        return (m, s) -> {
            if (!m.containsKey(s)) {
                m.put(s, new AtomicInteger());
            }
            int i = m.get(s).incrementAndGet();
            log.info("status: {}, quantity: {} ", s, i);
            return m;
        };
    }

    @Test
    public void testByKrzysztof() {
        Mono<Map<Status, Long>> mapMono = getIds()
                .flatMapIterable(Function.identity())
                .flatMap(this::getDetails)
                .groupBy(Details::getStatus, Function.identity())
                .flatMap(group -> Mono.zip(
                        Mono.just(group.key()),
                        group.count()))
                .collectMap(Tuple2::getT1, Tuple2::getT2 );

        mapMono.subscribe(m -> log.info("map: {}", m));
    }
}
