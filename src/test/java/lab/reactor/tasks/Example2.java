package lab.reactor.tasks;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;

public class Example2 {

    @Test
    void testExample2(){

        Object cat = new Object();

        Mono.just(1) // auth.getAuth(id)
                .flatMap( i -> Mono.just(i % 2==0) ) // access.checkAccess(i, cat)
                .filter(ac->ac) // if (ac) {
                .subscribe(
                        ac ->
                            Mono.just(102) //catalogue.getCards(cat)
                                    .subscribe(
                                            c -> Mono.just(c*2) //user.getUser(c)
                                                    .subscribe()
                                    )

                );



    }
                    /*
                    auth.getAuth(id).subscribe(
                i -> {
                    access.checkAccess(i, cat).subscribe(
                            ac -> {
                                if (ac) {
                                    catalogue.getCards(cat).subscribe(
                                        c -> {
                                              user.getUser(c).subscribe();
                                            }
                                        }
                                    );
                                }
                            }
                    );
                }
        );
                     */
}
