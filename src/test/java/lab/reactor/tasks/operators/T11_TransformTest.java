package lab.reactor.tasks.operators;

import lab.news.domain.News;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

import static org.junit.jupiter.api.Assertions.*;

class T11_TransformTest {


    Flux<News> flux = Flux.just(News.PANDEMIC_ENDS, News.POLAND_WON_MUNDIAL, News.INFLATION_DROPPED);

    T11_Transform transform = new T11_Transform();
    @Test
    void capitalizeTitles() {

        Iterable<String> iterable = transform.capitalizeTitles(flux)
                .map(news->news.getHeadline())
                .toIterable();

        Assertions.assertThat(iterable)
                .containsExactly(
                        News.PANDEMIC_ENDS.getHeadline().toUpperCase(),
                        News.POLAND_WON_MUNDIAL.getHeadline().toUpperCase(),
                        News.INFLATION_DROPPED.getHeadline().toUpperCase());

    }

    @Test
    void prefixTitleWithCategory() {
    }

    @Test
    void prefixTitleWithIndex() {

        transform.prefixTitleWithIndex(flux)
                .subscribe(n-> System.out.println("news: " + n));

    }

    @Test
    void prefixTitleWithTimestamp() {

        transform.prefixTitleWithTimestamp(flux)
                .subscribe(n-> System.out.println("news: " + n));

    }

    @Test
    void generateChessboard() throws Exception {
        transform.generateChessboard()
                .subscribe(n-> System.out.println("field: " + n));

    }
}