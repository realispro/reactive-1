package lab.reactor.tasks.basics;

import lab.news.domain.Category;
import lab.news.domain.News;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

class T01_FluxTest {

    private final T01_Flux classUnderTest = new T01_Flux();

    @Test
    void fluxEmpty() {
        //given
        final Flux<News> flux = classUnderTest.fluxEmpty();

        //when
        final List<News> value = flux.collectList().block();

        //then
        // v1:
        assertThat(value).isEmpty();

        // v2:
        StepVerifier.create(flux)
                .expectNextCount(0)
                .expectComplete()
                .verify();
    }

    @Test
    void fluxFromValues() {
        News news = new News(Category.POLITICS, "headline text", "some text");
        final Flux<News> flux = classUnderTest.fluxFromValues(news);

        //when
        final List<News> value = flux.collectList().block();

        //then
        assertThat(value).containsExactly(news);
    }

    @Test
    void fluxFromList() {

        List<News> newsList = List.of(News.PANDEMIC_ENDS, News.OSCARS_CEREMONY);

        final Flux<News> flux = classUnderTest.fluxFromList(newsList);

        //when
        final List<News> value = flux.collectList().block();

        //then
        assertThat(value).containsExactly(News.PANDEMIC_ENDS, News.OSCARS_CEREMONY);
    }

    @Test
    void fluxEmittingError() {
        //given
        final Flux<News> flux = classUnderTest.fluxEmittingError();

        //when
        try {
            flux.collectList().block();
            failBecauseExceptionWasNotThrown(UnsupportedOperationException.class);
        } catch (UnsupportedOperationException e) {
            //then
            assertThat(e).hasMessage("error cause");
        }
    }
}