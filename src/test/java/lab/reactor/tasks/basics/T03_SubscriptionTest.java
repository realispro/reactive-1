package lab.reactor.tasks.basics;

import lab.news.domain.News;
import lab.utils.Sleeper;
import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscriber;
import reactor.core.publisher.Flux;
import reactor.test.publisher.TestPublisher;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class T03_SubscriptionTest {

    private T03_Subscription classUnderTest = new T03_Subscription();

    @Test
    void twoItemsSubscriber() {
        //given
        Flux<News> newsFlux = Flux.fromIterable(
                List.of(News.PANDEMIC_ENDS, News.INFLATION_DROPPED, News.INFLATION_DROPPED));
        //when
        //then
        newsFlux
//                .log()
                .doOnNext(news -> System.out.println("Category: " + news.getCategory().toString()))
//                .log()
                .subscribe(classUnderTest.twoItemsSubscriber());
    }

    @Test
    void batchSubscriber() {
        //given
        Flux<News> newsFlux = Flux.fromIterable(List.of(
                News.POLAND_WON_MUNDIAL,
                News.OSCARS_CEREMONY,
                News.PANDEMIC_ENDS,
                News.INFLATION_DROPPED,
                News.POLAND_WON_MUNDIAL,
                News.OSCARS_CEREMONY,
                News.PANDEMIC_ENDS,
                News.INFLATION_DROPPED,
                News.POLAND_WON_MUNDIAL,
                News.OSCARS_CEREMONY,
                News.PANDEMIC_ENDS,
                News.INFLATION_DROPPED
        ));
        //when
        //then
        newsFlux
//                .log()
                .doOnNext(news -> System.out.println("Category: " + news.getCategory().toString()))
//                .log()
                .subscribe(classUnderTest.batchSubscriber(4, 9));
    }
}