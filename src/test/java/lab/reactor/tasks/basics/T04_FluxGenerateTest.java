package lab.reactor.tasks.basics;

import lab.news.domain.News;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.*;

class T04_FluxGenerateTest {

    T04_FluxGenerate testClass = new T04_FluxGenerate();

    @Test
    void fluxInfinite() {
        Flux<News> newsFlux = testClass.fluxInfinite();
        //then
        StepVerifier.create(newsFlux)
                .expectSubscription()
                .expectNextCount(1000)
                .thenCancel()
                .verify();
    }

    @Test
    void fluxRepeated() {
        //given
        //when
        Flux<News> newsFlux = testClass.fluxRepeated();
        //then
        StepVerifier.create(newsFlux)
                .expectSubscription()
                .expectNextCount(10)
                .verifyComplete();
    }


}