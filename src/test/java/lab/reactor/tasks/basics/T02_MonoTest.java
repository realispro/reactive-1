package lab.reactor.tasks.basics;

import lab.news.domain.News;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class T02_MonoTest {

    private T02_Mono t02Mono = new T02_Mono();

    @Test
    void emptyMono() {
        //given
        News block = t02Mono.emptyMono().block();
        //then
        Assertions.assertThat(block).isNull();
    }

    @Test
    void monoWithNoSignal() {
        //given
        try {
            t02Mono.monoWithNoSignal().block(Duration.ofSeconds(2));
            Assertions.failBecauseExceptionWasNotThrown(IllegalStateException.class);
        }catch (IllegalStateException e){
            Assertions.assertThat(e.getMessage()).startsWith("Timeout");
        }
        //then

    }

    @Test
    void valueMono() {
        //given
        //when
        News block = t02Mono.valueMono().block();
        //then
        Assertions.assertThat(block.getHeadline()).isEqualTo("Poland team won Mundial");

    }

    @Test
    void errorMono() {
        //given
        //when
        //then
        Assertions
                .assertThatThrownBy(() -> t02Mono.errorMono().block())
                .isInstanceOf(IllegalArgumentException.class).hasMessage("test exception");
    }
}