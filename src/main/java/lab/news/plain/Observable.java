package lab.news.plain;

public interface Observable {
    void addObserver(Observer observer);
}
