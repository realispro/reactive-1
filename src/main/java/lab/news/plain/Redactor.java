package lab.news.plain;

import lab.news.domain.Category;
import lab.news.domain.News;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Redactor {

    private static final Logger log = LoggerFactory.getLogger(Redactor.class);

    private final Agency agency;

    public Redactor(Agency agency) {
        this.agency = agency;
    }

    public void writeArticle(Category category, String title, String text){
        News news = agency.broadcast(category, title, text);
        log.info("article created: {}", news);
    }
}
