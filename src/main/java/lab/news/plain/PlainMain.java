package lab.news.plain;

import lab.news.domain.Category;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PlainMain {


    public static void main(String[] args) {

        Agency newsweek = new Agency("Newsweek");
        Redactor halik = new Redactor(newsweek);

        Reader reader = new Reader();
        reader.addSource(newsweek);

        halik.writeArticle(Category.SPORT, "Tribes Olimpics begun!", "New tournament has been started!");
        //reader.pollNews();

        halik.writeArticle(Category.POLITICS, "EU grows!", "Unknown island joins EU!");
        //reader.pollNews();





    }
}
