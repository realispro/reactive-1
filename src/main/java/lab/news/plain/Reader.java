package lab.news.plain;


import lab.news.domain.News;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

// Observer aka Subscriber
@Slf4j
public class Reader implements Subscriber<News> {

    public void addSource(Agency agency){
        agency.subscribe(this);
    }

    @Override
    public void onNext(News news){
        log.info("news update: {}", news);
    }

    @Override
    public void onSubscribe(Subscription subscription) {
    }


    @Override
    public void onError(Throwable throwable) {
    }

    @Override
    public void onComplete() {
    }


}
