package lab.news.plain;

import lab.news.domain.News;

public interface Observer {
    void update(News news);
}
