package lab.news.domain;

public class News {

    public static final News PANDEMIC_ENDS = new News(Category.POLITICS, "Pandemic is over", "The shares of company Pfizer have fallen by 50% ");
    public static final News INFLATION_DROPPED = new News(Category.POLITICS, "Inflation dropped", "The inflation rate has dropped to 5%.");
    public static final News POLAND_WON_MUNDIAL = new News(Category.SPORT, "Poland team won Mundial", "The final match Poland team beat Brasil 5:0");
    public static final News OSCARS_CEREMONY = new News(Category.ENTERTAINMENT, "Oscar ceremony behind us", "All awards are taken by Brad Pit");

    private Category category;
    private String headline;
    private String text;

    public News(Category category, String headline, String text) {
        this.category = category;
        this.headline = headline;
        this.text = text;
    }

    public Category getCategory() {
        return category;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "News{" +
                "category=" + category +
                ", headline='" + headline + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
