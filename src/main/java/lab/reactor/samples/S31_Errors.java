package lab.reactor.samples;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class S31_Errors {

    public static void main(String[] args) throws Exception {
        log.info("Let's handle errors!");

        S31_Errors errors = new S31_Errors();
        errors.onErrorReturn();
        errors.onErrorResume();

        log.info("done.");
    }

    public void onErrorReturn() throws Exception {
        //given
        final Mono<String> err = Mono.error(new RuntimeException("Opps"));

        //when
        final Mono<String> withFallback = err.onErrorReturn("Fallback");

        //then
        err
                .as(StepVerifier::create)
                .verifyErrorMessage("Opps");
        withFallback
                .as(StepVerifier::create)
                .expectNext("Fallback")
                .verifyComplete();
    }

    public void onErrorResume() throws Exception {
        //given
        AtomicBoolean cheapFlag = new AtomicBoolean();
        AtomicBoolean expensiveFlag = new AtomicBoolean();

        Mono<String> cheapButDangerous = Mono.fromCallable(() -> {
            cheapFlag.set(true);
            throw new RuntimeException("Failed");
        });

        Mono<String> expensive = Mono.fromCallable(() -> {
            expensiveFlag.set(true);
            return "Expensive";
        });

        //when
        final Mono<String> withError = cheapButDangerous
                .onErrorResume(e -> expensive);

        //then
        withError
                .as(StepVerifier::create)
                .expectNext("Expensive")
                .verifyComplete();
        assertThat(cheapFlag).isTrue();
        assertThat(expensiveFlag).isTrue();
    }

}
