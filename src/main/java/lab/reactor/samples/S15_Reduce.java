package lab.reactor.samples;


import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.assertj.core.api.Assertions.assertThat;


@Slf4j
public class S15_Reduce {

    public static void main(String[] args) throws Exception {
        log.info("Let's reduce!");

        S15_Reduce reduce = new S15_Reduce();

        reduce.sumUsingReduce();
//        reduce.factorialUsingReduce();
//        reduce.reduceIsEmptyOnEmptyStream();
//        reduce.scanFibonacci();
//        reduce.detectPrime();

        log.info("done.");
    }
    public void sumUsingReduce() throws Exception {
        //given
        final Flux<Integer> nums = Flux.range(1, 10);

        //when
        final Mono<Integer> sum = nums.reduce((accumulator, element) -> {
            log.info("accumulator = {}, element = {}", accumulator, element);
            return accumulator + element;
        });

        //then
        assertThat(sum.block()).isEqualTo(55);
    }


    public void factorialUsingReduce() throws Exception {
        //given
        final Flux<Integer> nums = Flux.range(1, 10);

        //when
        final Mono<Integer> factorial = nums.reduce((l, r) -> l * r);

        //then
        assertThat(factorial.block()).isEqualTo(
                ((((((((1 * 2) * 3) * 4) * 5) * 6) * 7) * 8) * 9) * 10);
    }

    public void reduceIsEmptyOnEmptyStream() throws Exception {
        //given
        final Flux<Integer> nums = Flux.empty();

        //when
        final Mono<Integer> sum = nums.reduce((accumulator, element) -> {
            log.info("accumulator = {}, element = {}", accumulator, element);
            return accumulator + element;
        });

        //then
        assertThat(sum.switchIfEmpty(Mono.just(-1)).block()).isEqualTo(-1);
    }


    public void scanIncreasing() throws Exception{

        final Flux<Integer> range = Flux.range(1, 10);

        //when
        final Flux<Integer> increasing = range.scan((l, r) -> l + r );

        assertThat(increasing.collectList().block()).containsExactly(1, 3, 6, 10, 15, 21, 28, 36, 45, 55);
    }

    public void detectPrime() throws Exception{
        final Flux<Integer> range = Flux.range(5, 5);

        Mono<Boolean> primeMono = range.all(n->{
            for (int i = 2; i < n; i++) {
                if (n % i == 0) {
                    return false;
                }
            }
            return true;
        });

        assertThat(primeMono.block()).isEqualTo(false);

    }


}
