package lab.reactor.samples;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.concurrent.atomic.AtomicInteger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

@Slf4j
public class S02_Mono {


	public static void main(String[] args) throws Exception {
		log.info("Let's meet mono!");

		S02_Mono mono = new S02_Mono();

		mono.helloMono();
		mono.emptyMono();
		mono.errorMono();
		mono.monoIsEager();
		mono.monoIsLazy();
		mono.lazyWithoutCaching();
		mono.cachingMonoComputesOnlyOnce();

		System.out.println("done.");
	}



	public void helloMono() throws Exception {
		//given
		final Mono<String> reactor = Mono.just("Reactor");

		//when
		final String value = reactor.block();

		//then
		assertThat(value).isEqualTo("Reactor");
	}

	public void emptyMono() throws Exception {
		//given
		final Mono<String> reactor = Mono.empty();

		//when
		final String value = reactor.block();

		//then
		assertThat(value).isNull();
	}

	public void errorMono() throws Exception {
		//given
		final Mono<String> error = Mono.error(new UnsupportedOperationException("Simulated"));

		//when
		try {
			error.block();
			failBecauseExceptionWasNotThrown(UnsupportedOperationException.class);
		} catch (UnsupportedOperationException e) {
			//then
			assertThat(e).hasMessage("Simulated");
		}
	}

	public void monoIsEager() throws Exception {
		//given
		AtomicInteger counter = new AtomicInteger();

		//when
		Mono.just(counter.incrementAndGet());

		//then
		assertThat(counter).hasValue(1);
	}

	public void monoIsLazy() throws Exception {
		//given
		AtomicInteger counter = new AtomicInteger(0);

		//when
		Mono.fromCallable(() -> counter.incrementAndGet());

		//then
		assertThat(counter).hasValue(0);
	}

	public void lazyWithoutCaching() throws Exception {
		//given
		AtomicInteger counter = new AtomicInteger(0);
		final Mono<Integer> lazy = Mono.fromCallable(() -> counter.incrementAndGet());

		//when
		final Integer first = lazy.block();
		final Integer second = lazy.block();

		//then
		assertThat(first).isEqualTo(1);
		assertThat(second).isEqualTo(2);
	}

	public void cachingMonoComputesOnlyOnce() throws Exception {
		//given
		AtomicInteger counter = new AtomicInteger(0);
		final Mono<Integer> lazy = Mono.fromCallable(counter::incrementAndGet).cache();

		//when
		lazy.block();
		lazy.block();

		//then
		assertThat(counter).hasValue(1);
	}

}
