package lab.reactor.samples;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.GroupedFlux;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class S14_Collect {

    public static void main(String[] args) {
        log.info("Let's collect");

        S14_Collect collect = new S14_Collect();

        collect.collectMonoList();
        collect.toIterable();
        collect.collectMap();
        collect.collectMultiMap();

        log.info("done.");
    }

    private static final List<String> words = List.of(
            "lorem", "ipsum", "dolor", "sit", "amet", "consectetur", "adipiscing",
            "elit", "curabitur", "vel", "hendrerit");


    public void collectMonoList() {

        Flux<String> flux = Flux.fromIterable(words);

        Mono<List<String>> monoList = flux.collectList();

        assertThat(monoList.block()).containsExactly(words.toArray(new String[0]));
    }

    public void toIterable() {

        Flux<String> flux = Flux.fromIterable(words);

        Iterable<String> iterable = flux.toIterable();

        assertThat(iterable).containsExactly(words.toArray(new String[0]));
    }

    public void collectMap() {

        Flux<String> flux = Flux.fromIterable(words);

        Mono<Map<Integer, String>> monoMap = flux
                .collectMap(w->w.length())
                .doOnNext(m->log.info("map: {}", m));

        assertThat(monoMap.block().size()).isEqualTo(6);
    }

    public void collectMultiMap() {

        Flux<String> flux = Flux.fromIterable(words);

        Mono<Map<Integer, Collection<String>>> monoMap = flux
                .collectMultimap(w->w.length())
                .doOnNext(m->log.info("map: {}", m));

        assertThat(monoMap.block().size()).isEqualTo(6);
    }


    public void groupBy(){
        Flux<String> flux = Flux.fromIterable(words);
        final Flux<GroupedFlux<Integer, String>> wordsByLength = flux.groupBy(String::length);

        //when
        final Flux<Long> numOfWords = wordsByLength.flatMap(words -> words.count());
        final Flux<Integer> wordLengths = wordsByLength.map(words -> words.key());

    }
}
