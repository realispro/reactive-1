package lab.reactor.samples;

import lab.utils.CacheServer;
import lab.utils.Sleeper;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class S21_Scheduler {

    public static void main(String[] args) throws Exception {
        log.info("Let's schedule!");

        S21_Scheduler scheduler = new S21_Scheduler();

//        scheduler.sameThread();
//        scheduler.subscribeOn();
//        scheduler.manyStreamsButLastThreadWins();
//        scheduler.publishOn();
//        scheduler.ignoredSubscribeOn();
        scheduler.guessWhichOne();


        log.info("done.");
    }

    private final CacheServer reliable = new CacheServer("foo", Duration.ofMillis(1_000), 0);

    public void sameThread() throws Exception {

        log.info("*** Starting sameThread()");
        final Mono<String> one = Mono.fromCallable(() -> reliable.findBlocking(41));
        final Mono<String> two = Mono.fromCallable(() -> reliable.findBlocking(42));

        one.subscribe(x -> log.info("Got from one: {}", x));
        log.info("Got first response");
        two.subscribe(x -> log.info("Got from two: {}", x));
        log.info("Got second response");
    }

    public void subscribeOn() throws Exception {
        log.info("*** Starting subscribeOn()");
        Scheduler scheduler = Schedulers.newBoundedElastic(10, 100, "SubscribeOn");
        Mono
                .fromCallable(() -> reliable.findBlocking(41))
                //.delayElement(Duration.ofSeconds(1))
                .subscribeOn(scheduler)
                .doOnNext(x -> log.info("Received {}", x))
                .map(x -> {
                    log.info("Mapping {}", x);
                    return x;
                })
                .filter(x -> {
                    log.info("Filtering {}", x);
                    return true;
                })
                .doOnNext(x -> log.info("Still here {}", x))
                .subscribe(x -> log.info("Finally received {}", x));

        scheduler.dispose();

        Sleeper.sleep(Duration.ofSeconds(5));
    }

    public void manyStreamsButLastThreadWins() throws Exception {
        log.info("*** Starting manyStreamsButLastThreadWins()");
        final Mono<String> one = Mono.fromCallable(() -> reliable.findBlocking(41));
        final Mono<String> two = Mono.fromCallable(() -> reliable.findBlocking(42));

        Mono
                .zip(
                        one.subscribeOn(Schedulers.newBoundedElastic(10, 100, "A")),
                        two.subscribeOn(Schedulers.newBoundedElastic(10, 100, "B"))
                )
                .doOnNext(x -> log.info("Received {}", x))
                .map(x -> {
                    log.info("Mapping {}", x);
                    return x;
                })
                .filter(x -> {
                    log.info("Filtering {}", x);
                    return true;
                })
                .doOnNext(x -> log.info("Still here {}", x))
                .subscribe(x -> log.info("Finally received {}", x));

        TimeUnit.SECONDS.sleep(2);
    }

    public void publishOn() throws Exception {
        log.info("*** Starting publishOn()");

        Mono
                .fromCallable(() -> reliable.findBlocking(41))
                .subscribeOn(Schedulers.newBoundedElastic(10, 100, "A"))
                .doOnNext(x -> log.info("Received {}", x))
                .subscribeOn(Schedulers.newBoundedElastic(10, 100, "A-prim"))
                .publishOn(Schedulers.newBoundedElastic(10, 100, "B"))
                .map(x -> {
                    log.info("Mapping {}", x);
                    return x;
                })
                .publishOn(Schedulers.newBoundedElastic(10, 100, "C"))
                .filter(x -> {
                    log.info("Filtering {}", x);
                    return true;
                })
                .publishOn(Schedulers.newBoundedElastic(10, 100, "D"))
                .doOnNext(x -> log.info("Still here {}", x))
                .publishOn(Schedulers.newBoundedElastic(10, 100, "E"))
                .subscribe(x -> log.info("Finally received {}", x));

        TimeUnit.SECONDS.sleep(2);
    }

    public void ignoredSubscribeOn(){

        log.info("*** Starting ignoredSubscribeOn()");

        Flux.just("hello")
                .delayElements(Duration.ofMillis(500))
                .subscribeOn(Schedulers.single())
                .subscribe(s -> log.info("delayed {}", s));


        Sleeper.sleep(Duration.ofSeconds(1));
    }

    public void guessWhichOne(){
//
        log.info("*** Starting guessWhichOne()");

        Flux.just("hello")
                .doOnNext(s -> log.info("just {}", s))
                .publishOn(Schedulers.boundedElastic())
                .doOnNext(s -> log.info("publish {}", s))
                .delayElements(Duration.ofMillis(500))
                .subscribeOn(Schedulers.single())
                .subscribe(s -> log.info("delayed {}", s));


        Sleeper.sleep(Duration.ofSeconds(5));
    }

    private Scheduler customScheduler() {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        return Schedulers.fromExecutorService(executorService);
    }
}
