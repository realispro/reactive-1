package lab.reactor.samples;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

@Slf4j
public class S01_Flux {

    public static void main(String[] args) throws Exception {
        log.info("Let's meet flux!");

        S01_Flux flux = new S01_Flux();

        flux.helloFlux();
        flux.emptyFlux();
        flux.manyValues();
        flux.errorFlux();
        flux.fluxIsEager();
        flux.fluxIsLazy();
        flux.fluxComputesManyTimes();
        flux.makeLazyComputeOnlyOnce();

        log.info("done.");

    }

    public void helloFlux() throws Exception {
        //given
        final Flux<String> reactor = Flux.just("Hello");

        //when
        final List<String> value = reactor.collectList().block();

        //then
        assertThat(value).containsExactly("Hello");
    }


    public void emptyFlux() throws Exception {
        //given
        final Flux<String> reactor = Flux.empty();

        //when
        final List<String> value = reactor.collectList().block();

        //then
        assertThat(value).isEmpty();
    }

    public void manyValues() throws Exception {
        //given
        final Flux<String> reactor = Flux.just("Hello", "world");

        //when
        final List<String> value = reactor.collectList().block();

        //then
        assertThat(value).containsExactly("Hello", "world");
    }

    public void errorFlux() throws Exception {
        //given
        final Flux<String> error = Flux.error(new UnsupportedOperationException("Simulated"));

        //when
        try {
            error.collectList().block();
            failBecauseExceptionWasNotThrown(UnsupportedOperationException.class);
        } catch (UnsupportedOperationException e) {
            //then
            assertThat(e).hasMessage("Simulated");
        }
    }



    public void fluxIsEager() throws Exception {
        //given
        AtomicInteger counter = new AtomicInteger();

        //when
        Flux.just(counter.incrementAndGet(), counter.incrementAndGet());

        //then
        assertThat(counter).hasValue(2);
    }

    public void fluxIsLazy() throws Exception {
        //given
        AtomicInteger c = new AtomicInteger();

        //when
        Flux<Integer> integerFlux = Flux.fromStream(() -> Stream.of(c.incrementAndGet(), c.incrementAndGet()));

        /*integerFlux
                .subscribe( i -> log.info("i: {}", i) );*/

        //then

        assertThat(c.get()).isZero();
    }

    public void fluxComputesManyTimes() throws Exception {
        //given
        AtomicInteger c = new AtomicInteger();
        final Flux<Integer> flux = Flux.fromStream(() ->
                Stream.of(c.incrementAndGet(), c.incrementAndGet()));

        //when
        final List<Integer> first = flux.collectList().block();
        final List<Integer> second = flux.collectList().block();

        //then
        assertThat(c).hasValue(4);
        assertThat(first).containsExactly(1, 2);
        assertThat(second).containsExactly(3, 4);
    }

    public void makeLazyComputeOnlyOnce() throws Exception {
        //given
        AtomicInteger c = new AtomicInteger();
        Flux<Integer> flux = Flux.fromStream(() ->
                        Stream.of(c.incrementAndGet(), c.incrementAndGet()))
                .cache();

        //when
        final List<Integer> first = flux.collectList().block();
        final List<Integer> second = flux.collectList().block();

        //then
        assertThat(c).hasValue(2);
        assertThat(first).isEqualTo(second);
    }
}
