package lab.reactor.samples;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.concurrent.TimeoutException;

import static java.time.Duration.ofMillis;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

@Slf4j
public class S17_Timing {

    public static void main(String[] args) throws Exception {
        log.info("Let's check timing!");

        S17_Timing timing = new S17_Timing();

  //      timing.timeout();
        timing.timeoutFallback();
        timing.timeoutRetry();
//        timing.delayAll();

        log.info("done.");
    }

    public void timeout() throws Exception {
        //given
        final Mono<Long> withTimeout = Mono.just(1L).delayElement(ofMillis(200));

        //when
        try {
            withTimeout
                    .timeout(ofMillis(100))
                    .block();
            failBecauseExceptionWasNotThrown(TimeoutException.class);
        }catch (Exception e){
            log.info("exception: {}", e.getMessage() );
        }

    }

    public void timeoutFallback() throws Exception {
        //given
        final Mono<Long> withTimeout = Mono.just(1L).delayElement(ofMillis(200));

        //when
        Mono<Long> fallback = withTimeout
                    .timeout(ofMillis(100), Mono.just(-1L));

        assertThat(fallback.block()).isEqualTo(-1L);

    }

    public void timeoutRetry() throws Exception {
        //given
        final Mono<Long> withTimeout = Mono.delay(Duration.ofMillis(200));

        //when
        try {
            Long value = withTimeout
                    .timeout(ofMillis(300))
                    .retry(1)
                    //.timeout(ofMillis(300))
                    .block();
            log.info("value: {}", value);
            failBecauseExceptionWasNotThrown(TimeoutException.class);
        }catch (Exception e){
            log.info("exception: {}", e.getMessage() );
        }

    }


    public void delayAll() throws Exception {

        //given
        final Flux<Integer> delayed = Flux.range(1, 5)
                .delayElements(Duration.ofSeconds(2));

        //when
        final Flux<Integer> timeout = delayed.timeout(Duration.ofSeconds(1));

        try {
            //then
            timeout.blockLast();
            failBecauseExceptionWasNotThrown(TimeoutException.class);
        }catch (Exception e){
            log.info("exception: {}", e.getMessage() );
        }
    }


}
