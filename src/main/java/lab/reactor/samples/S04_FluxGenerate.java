package lab.reactor.samples;

import lab.utils.Sleeper;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.core.publisher.SynchronousSink;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class S04_FluxGenerate {

	public static void main(String[] args) throws Exception {
		log.info("Let's generate flux!");

		S04_FluxGenerate fluxGenerate = new S04_FluxGenerate();

//		fluxGenerate.generateRandomStreamOfNumbers();
//		fluxGenerate.statefulGenerate();
		fluxGenerate.fibonacci();
		fluxGenerate.generatedFluxOfLines();
		fluxGenerate.createdFluxOfLines();

		log.info("done.");
	}


	public void generateRandomStreamOfNumbers() throws Exception {
		//given
		final Flux<Float> randoms = Flux
				.generate(sink -> {
					final float rand = new Random(System.currentTimeMillis()).nextFloat();
							//ThreadLocalRandom.current().nextFloat();
					log.debug("Returning {}", rand);
					sink.next(rand);
				});

		//when
		final Flux<Float> twoRandoms = randoms
				.take(20);

		//then
		twoRandoms.subscribe(f->log.info("float: {}", f));
	}


	public void statefulGenerate() throws Exception {
		//given
		final Flux<Integer> naturals = Flux.generate(() -> 0, (state, sink) -> {
			if(state==3){
				sink.complete();
			}
			sink.next(state);

			return state + 1;
		});

		//when
		final Flux<Integer> three = naturals;//.take(3);

		three.subscribe(
				i->log.info("statefulGenerate: {}", i),
				null,
				()->log.info("statefulGenerate complete."));
		//then
		assertThat(three.collectList().block()).containsExactly(0, 1, 2);
	}



	public void fibonacci() throws Exception {
		//given
		final Flux<Long> fib = Flux.generate(
				() -> 1,
				(p, sink) -> {

					long n1=0,n2=1,n3,counter=1;
					while(counter<=p){
						n3=n1+n2;
						n1=n2;
						n2=n3;
						counter++;
					}
					sink.next(n2);
					return p+1;

				});

		//when
		final Flux<Long> first10 = fib.take(10);

		//then
		assertThat(first10.collectList().block()).containsExactly(1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L, 55L, 89L);
	}


	public void generatedFluxOfLines() throws Exception {
		//given
		Flux<String> lines = Flux
				.generate(
						() -> new BufferedReader(new InputStreamReader(
								this.getClass().getResourceAsStream("/fluxGenerate.data"))),
						(reader, sink) -> {
							readLine(reader, sink);
							return reader;
						},
						this::closeQuitely);

		//when


		//then
		final List<String> list = lines
				.collectList()
				.block();

		assertThat(list).hasSize(4);
	}

	public void createdFluxOfLines() throws Exception {

		log.info("createdFluxOfLines started");
		//given
		Flux<String> lines = Flux
				.create(sink->{
					try(BufferedReader br = new BufferedReader(new InputStreamReader(
							this.getClass().getResourceAsStream("/fluxGenerate.data")))){
						String line;
						while((line=br.readLine())!=null){
							if(sink.isCancelled()){
								log.info("createdFluxOfLines: emitting rejected because sinnk canceled");
								return;
							}
							log.info("createdFluxOfLines: emitting next item: {}", line);
							sink.next(line);
						}
						sink.complete();
					} catch (IOException e) {
                        sink.error(e);
                    }
                });

		//when

		/*lines.subscribe(line->{
			Sleeper.sleep(Duration.ofSeconds(2));
			log.info("createdFluxOfLines: received item {}", line);
		});*/

		lines.subscribe(new BaseSubscriber<String>() {
			@Override
			protected void hookOnNext(String line) {
				Sleeper.sleep(Duration.ofSeconds(2));
				log.info("createdFluxOfLines: received item {}", line);
				if(line.equals("second line")){
					log.info("another line detected. cancelling...");
					cancel();
				}
			}
		});



		//then
		/*final List<String> list = lines
				.collectList()
				.block();

		assertThat(list).hasSize(4);*/
	}



	private void closeQuitely(AutoCloseable closeable) {
		try {
			log.info("Closing {}", closeable);
			closeable.close();
		} catch (Exception e) {
			log.warn("Unable to close {}", closeable, e);
		}
	}

	private void readLine(BufferedReader file, SynchronousSink<String> sink)  {
		try {
			String line = file.readLine();
			if(line!=null){
				sink.next(line);
			} else {
				sink.complete();
			}
		} catch (IOException e) {
			sink.error(e);
		}
	}

}
