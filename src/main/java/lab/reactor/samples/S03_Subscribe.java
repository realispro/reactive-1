package lab.reactor.samples;

import lab.utils.Sleeper;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Subscription;
import reactor.core.Disposable;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;


@Slf4j
public class S03_Subscribe {

	public static void main(String[] args) throws Exception {
		log.info("Let's subscribe!");

		S03_Subscribe subscribe = new S03_Subscribe();

//		subscribe.noWorkHappensWithoutSubscription();
//		subscribe.blockTriggersWork();
//		subscribe.subscriptionTriggersWork();
//		subscribe.subscriptionOfManyNotifications();
//		subscribe.fluxCompletingWithError();
//		subscribe.fluxCompletingWithoutAnyValue();
		subscribe.subscriptionAllSignals();
//		subscribe.subscriptionAllSignalsImplementingSubscriber();

		log.info("done");
	}


	public void noWorkHappensWithoutSubscription() throws Exception {
		//given
		AtomicBoolean flag = new AtomicBoolean();

		//when
		log.info("About to create Flux");
		Flux.fromStream(() -> {
			log.info("Doing hard work");
			flag.set(true);
			return Stream.of(1, 2, 3);
		});

		//then
		assertThat(flag).isFalse();
	}


	public void blockTriggersWork() throws Exception {
		//given
		AtomicBoolean flag = new AtomicBoolean();

		//when
		log.info("About to create Flux");
		final Flux<Integer> work = Flux.fromStream(() -> {
			log.info("Doing hard work");
			flag.set(true);
			return Stream.of(1, 2, 3);
		});
		log.info("Flux was created");
		final Integer result = work.blockLast();
		log.info("Work is done");

		//then
		assertThat(flag).isTrue();
		assertThat(result).isEqualTo(3);
	}

	public void subscriptionTriggersWork() throws Exception {
		//given
		AtomicBoolean flag = new AtomicBoolean();
		log.info("About to create Flux");

		//when
		final Flux<Integer> work = Flux.fromStream(() -> {
			log.info("Doing hard work");
			flag.set(true);
			return Stream.of(1, 2, 3);
		});

		//then
		log.info("Flux was created");

		work.subscribe(i -> log.info("Received {}", i));

		log.info("Work is done");
	}

	public void subscriptionOfManyNotifications() throws Exception {
		//given
		AtomicBoolean flag = new AtomicBoolean();
		log.info("About to create Flux");

		//when
		final Flux<Integer> work = Flux.fromStream(() -> {
			log.info("Doing hard work");
			flag.set(true);
			return Stream.of(1, 2, 3);
		});

		//then
		log.info("Flux was created");

		work.subscribe(
				i -> log.info("Received {}", i),
				ex -> log.error("Opps!", ex),
				() -> log.info("Flux completed")
		);

		log.info("Work is done");
	}

	public void subscriptionAllSignals(){
		Flux<Long> longs = Flux.interval(Duration.ofSeconds(1))
				.take(10);

		Disposable disposable = longs.subscribe(
				l -> log.info("long: {}", l),
				error -> log.error("error", error),
				() -> log.info("completed.")/*,
				subscription -> {
					log.info("subscription: {}", subscription);
					subscription.request(3);
				}*/
		);
		Sleeper.sleep(Duration.ofSeconds(5));

		disposable.dispose();

		Sleeper.sleep(Duration.ofSeconds(5));

		log.info("work is done.");
	}

	public void subscriptionAllSignalsImplementingSubscriber(){
		Flux<Long> longs = Flux.range(1, 10).map(i->(long)i).delayElements(Duration.ofSeconds(1));
				//.interval(Duration.ofSeconds(1)).take(10);

		longs.subscribe(
				new BaseSubscriber<>() {

					private int counter;

					@Override
					protected void hookOnNext(Long value) {
						log.info("hook next long: {}", value);
						counter++;
						if(counter==3){
							log.info("next chunk.");
							counter=0;
							request(3);
						}
						Sleeper.sleep(Duration.ofSeconds(5));
					}

					@Override
					protected void hookOnComplete() {
						log.info("hook stream completed.");
					}

					@Override
					protected void hookOnError(Throwable throwable) {
						log.error("hook error in stream", throwable);
					}

					@Override
					protected void hookOnSubscribe(Subscription subscription) {
						log.info("hook subscribe: {}", subscription);
						request(3);
					}

					@Override
					protected void hookOnCancel() {
						log.info("hook cancel.");
					}
				}
		);

		Sleeper.sleep(Duration.ofSeconds(15));

		log.info("work is done.");
	}



	public void fluxCompletingWithError() {
		//given
		final List<Integer> onNext = new CopyOnWriteArrayList<>();
		final AtomicReference<Throwable> error = new AtomicReference<>();
		final AtomicBoolean completed = new AtomicBoolean();


		//when
		final Flux<Integer> work = Flux.error(new IllegalArgumentException("Simulated"));

		//then
		work.subscribe(
				onNext::add,
				error::set,
				() -> completed.set(true)
		);

		//Hint: you don't normally test streams like that! Don't get used to it
		assertThat(onNext).isEmpty();
		assertThat(error.get())
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Simulated");
		assertThat(completed).isFalse();
	}

	public void fluxCompletingWithoutAnyValue() throws Exception {
		//given
		final List<Integer> onNext = new CopyOnWriteArrayList<>();
		final AtomicReference<Throwable> error = new AtomicReference<>();
		final AtomicBoolean completed = new AtomicBoolean();

		//when
		final Flux<Integer> work = Flux.empty();

		//then
		work.subscribe(
				onNext::add,
				error::set,
				() -> completed.set(true)
		);

		assertThat(onNext).isEmpty();
		assertThat(error).hasValue(null);
		assertThat(completed).isTrue();
	}


}
