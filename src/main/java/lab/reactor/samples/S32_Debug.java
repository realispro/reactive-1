package lab.reactor.samples;

import lab.news.domain.News;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Hooks;
import reactor.tools.agent.ReactorDebugAgent;
import reactor.util.Loggers;

public class S32_Debug {

    private static final Logger log = LoggerFactory.getLogger(S32_Debug.class);

    public static void main(String[] args) {
        log.info("Let's debug!");

        S32_Debug debug = new S32_Debug();
        debug.addLog();
        debug.addLogWithLogger();
        debug.spyEvents();

        log.info("done.");
    }

    public void addLog() {
        Flux<News> flux = Flux.just(News.PANDEMIC_ENDS, News.POLAND_WON_MUNDIAL);
        flux = Flux.concat(flux, Flux.error(new IllegalArgumentException("just checking")));
        flux
                .log()
                .subscribe();
    }

    public void addLogWithLogger() {
        Flux<News> flux = Flux.just(News.INFLATION_DROPPED, News.POLAND_WON_MUNDIAL);
        Loggers.useSl4jLoggers();
        flux
                .log(Loggers.getLogger(S32_Debug.class))
                .subscribe();
    }


    public void spyEvents() {
        Flux<News> flux = Flux.just(News.PANDEMIC_ENDS, News.POLAND_WON_MUNDIAL);

        flux
                .doOnSubscribe(s -> log.info("Spying subscription: " + s.toString()))
                .doOnNext(news -> log.info(news.getCategory() + " " + news.getHeadline()))
                .doOnComplete(() -> log.info("The end!"))
                .subscribe();
    }

}
