package lab.reactor.samples;

import lab.utils.CacheServer;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.time.Duration.ofMillis;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

@Slf4j
public class S16_Merge {

    public static void main(String[] args) throws Exception {
        log.info("Let's merge streams!");

        S16_Merge merge = new S16_Merge();

    //    merge.mergeCombinesManyStreams();
        merge.mergingMonos();
        merge.fetchDataFromFirstAvailableServer();
        merge.concatTwoFluxes();
        merge.errorFluxAfterValues();

        log.info("done.");
    }

    public void mergeCombinesManyStreams() throws Exception {
        //given
        final Flux<String> fast = Flux.interval(ofMillis(90)).map(x -> "F-" + x);
        final Flux<String> slow = Flux.interval(ofMillis(100)).map(x -> "S-" + x);

        //when
        final Flux<String> merged = Flux.merge(fast, slow);

        //then
        merged.subscribe(log::info);
        TimeUnit.SECONDS.sleep(2);
    }

    public void mergingMonos() throws Exception {
        //given
        final Mono<BigDecimal> fast = Mono.just(BigDecimal.valueOf(1))
                .delayElement(ofMillis(200));

        final Mono<BigDecimal> slow = Mono.just(BigDecimal.valueOf(2))
                .delayElement(ofMillis(100));

        //when
        final Flux<BigDecimal> merged = Flux.merge( fast, slow );

        //then
        merged.subscribe(d -> log.info("Received {}", d));
        TimeUnit.SECONDS.sleep(2);
    }


    public void fetchDataFromFirstAvailableServer() throws Exception {

        CacheServer foo = new CacheServer("foo.com", ofMillis(20), 0);
        CacheServer bar = new CacheServer("bar.com", ofMillis(20), 0);

        //given
        final Mono<String> fooResponse = foo.findBy(42);
        final Mono<String> barResponse = bar.findBy(42);

        //when
        Mono<String> fastest = Flux.merge(fooResponse, barResponse).next();

        //then
        fastest.subscribe(log::info);

    }

    public void concatTwoFluxes() throws Exception {
        //given
        final Flux<String> many = Flux.concat(
                Flux.just("Hello"),
                Flux.just("reactive", "world")
        );

        //when
        final List<String> values = many.collectList().block();

        //then
        assertThat(values).containsExactly("Hello", "reactive", "world");
    }

    public void errorFluxAfterValues() throws Exception {
        //given
        final Flux<String> error = Flux.concat(
                Flux.just("Hello", "world"),
                Flux.error(new UnsupportedOperationException("Simulated"))
        );

        //when
        try {
            error.collectList().block();
            failBecauseExceptionWasNotThrown(UnsupportedOperationException.class);
        } catch (UnsupportedOperationException e) {
            //then
            assertThat(e).hasMessage("Simulated");
        }
    }
}
