package lab.reactor.samples;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class S12_Filter {

    public static void main(String[] args) throws Exception {
        log.info("Let's filter!");

        S12_Filter filter = new S12_Filter();

        filter.filterSelectsOnlyMatchingElements();
        filter.distinctWords();
        filter.distinctLength();
        filter.distinctWordSequences();

        log.info("done.");
    }

    private static final Flux<String> words = Flux.just("elit", "elit", "est", "est", "eget", "et", "eget", "erat");


    public void filterSelectsOnlyMatchingElements() throws Exception {
        //given

        //when
        final Flux<String> endingWithT = words.filter(w->w.contains("i"));

        //then
        assertThat(endingWithT.collectList().block()).containsExactly("elit", "elit");
    }


    public void distinctWords() throws Exception {
        //when
        final Flux<String> distinct = words.distinct();

        //then
        assertThat(distinct.collectList().block()).containsExactly("elit", "est", "eget", "et", "erat");

    }

    public void distinctLength() throws Exception {
        //given

        //when
        final Flux<String> distinct = words.distinct(String::length);

        //then
        assertThat(distinct.collectList().block()).containsExactly("elit", "est", "et");
    }

    public void distinctWordSequences() throws Exception {
        //when
        final Flux<String> distinct = words.distinctUntilChanged();

        //then
        assertThat(distinct.collectList().block()).containsExactly("elit", "est", "eget", "et", "eget", "erat");
    }
}
