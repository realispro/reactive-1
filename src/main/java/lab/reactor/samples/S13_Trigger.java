package lab.reactor.samples;

import lab.utils.Sleeper;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;

@Slf4j
public class S13_Trigger {

    public static void main(String[] args) {
        log.info("Let's trigger!");

        S13_Trigger trigger = new S13_Trigger();

        //trigger.then();
        trigger.thenOnly();

        log.info("done.");
    }

    public void thenOnly(){


        Flux.interval(Duration.ofSeconds(1))
                .take(5)
                .doOnNext(i->log.info("tick: {}", i))
                .then()
                        .subscribe(
                                null,
                                null,
                                ()->log.info("mono void completed")
                        );
        log.info("got mono already");

        Sleeper.sleep(Duration.ofSeconds(7));



    }


    public void then() {


        Flux.just(true, false, true)
                .delayElements(Duration.ofSeconds(5))

                .doOnNext(b -> {
                    log.info("this is {}", b);
                    //Sleeper.sleep(Duration.ofSeconds(5));
                })
                .doOnComplete(() -> {
                    Sleeper.sleep(Duration.ofSeconds(5));
                    log.info("first flux completed");
                })

                .thenMany(Flux.fromIterable(List.of(1, 2, 3)))
                .subscribe(i -> log.info("second flux item: {}", i));

        Sleeper.sleep(Duration.ofSeconds(40));

    }

    public void when() {
        Mono.when(
                        Flux.range(1, 5)
                                .delayElements(Duration.ofSeconds(1))
                                .doOnNext(n -> {
                                    log.info("first stream item: {}", n);
                                    if(n==4){
                                        throw new IllegalArgumentException("simulated");
                                    }
                                }),
                        Flux.range(101, 5)
                                .delayElements(Duration.ofSeconds(3))
                                .doOnNext(n -> log.info("second stream item: {}", n)))
                .subscribe(
                        v->log.info("mono void: {}", v),
                        t->log.error("outer error", t),
                        () -> log.info("both streams finished"));

        Sleeper.sleep(Duration.ofSeconds(20));
    }
}
