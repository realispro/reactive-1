package lab.reactor.tasks.async;

import lab.news.domain.News;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

public class T21_Scheduler {

    // TODO Asyn1:1 assign elastic scheduler for incoming stream
    public Flux<News> assignScheduler(Flux<News> flux){
        return flux.subscribeOn(Schedulers.boundedElastic());
    }

    // TODO Async1:1 Generate all chessboard fields, each rows by separate worker
    public Flux<String> generateChessboard() throws Exception {

        final Flux<Integer> rows = Flux.range(1, 8);
        final Flux<String> cols = Flux.just("a", "b", "c", "d", "e", "f", "g", "h");

        return null;
    }
}
