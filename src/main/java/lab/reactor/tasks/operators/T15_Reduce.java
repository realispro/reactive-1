package lab.reactor.tasks.operators;

import lab.news.domain.News;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class T15_Reduce {

    // TODO Op5:1 find longest text included in Flux
    Mono<News> longestText(Flux<News> flux) {
        return null;
    }

    // TODO Op5:2 Signal true if any of news is SPORT category
    Mono<Boolean> containsSportNews(Flux<News> flux) {
        return null;
    }

    // TODO Op5:3 Signal true if none of news in flux is ENTERTAINMENT
    Mono<Boolean> doesNotContainEntertainment(Flux<News> flux) {
        return null;
    }
}
