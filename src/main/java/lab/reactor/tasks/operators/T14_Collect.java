package lab.reactor.tasks.operators;

import lab.news.domain.Category;
import lab.news.domain.News;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class T14_Collect {


    // TODO Op4:1 Convert the input Flux<News> to a Mono<List<News>> containing list of collected values
    Mono<List<News>> fluxCollection(Flux<News> flux) {
        return null;
    }

    // TODO Op4:2 Convert the input Flux<News> to a Mono<Map<Category, Collection<News>>> containing list of collected values
    Mono<Map<Category, Collection<News>>> fluxMap(Flux<News> flux) {
        return null;
    }


}
