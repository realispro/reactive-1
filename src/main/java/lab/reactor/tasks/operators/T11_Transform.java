package lab.reactor.tasks.operators;

import lab.news.domain.News;
import reactor.core.publisher.Flux;

public class T11_Transform {

    // TODO Op1:1 Capitalize titles
    public Flux<News> capitalizeTitles(Flux<News> flux) {
        return flux
                .map(news->{
                    news.setHeadline(news.getHeadline().toUpperCase());
                    return news;
                });
    }

    //TODO Op1:2 Prefix title with category in format `category: title`
    public Flux<News> prefixTitleWithCategory(Flux<News> flux) {
        return null;
    }

    //TODO Op1:3 Prefix title with index in format `index: title`
    public Flux<News> prefixTitleWithIndex(Flux<News> flux) {
        return flux.index((index,news) -> {
            news.setHeadline(index + ": " + news.getHeadline());
            return news;
        });
    }

    //TODO Op1:4 Prefix title with index in format `timestamp: title`
    public Flux<News> prefixTitleWithTimestamp(Flux<News> flux) {
        return flux
                .timestamp()
                .map(t->{
                    News news = t.getT2();
                    news.setHeadline(t.getT1() + ":" + news.getHeadline());
                    return news;
                });
    }

    // TODO Op1:5 Generate all chessboard fields
    public Flux<String> generateChessboard() throws Exception {

        final Flux<Integer> rows = Flux.range(1, 8);
        final Flux<String> cols = Flux.just("a", "b", "c", "d", "e", "f", "g", "h");

        return rows.flatMap(row->cols.map(col->col+row));
    }
}