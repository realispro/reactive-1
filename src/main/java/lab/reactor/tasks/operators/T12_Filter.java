package lab.reactor.tasks.operators;

import lab.news.domain.News;
import reactor.core.publisher.Flux;

public class T12_Filter {

    // TODO Op2:1 Remove ENTERTAINMENT news
    public Flux<News> removeEntertainmentNews(Flux<News> flux) {
        return null;
    }

    // TODO Op2:2 Remove duplicated news
    public Flux<News> removeDuplicatedNews(Flux<News> flux) {
        return null;
    }

    // TODO Op2:3 Remove news with duplicated title
    public Flux<News> removeNewsWithDuplicatedTitle(Flux<News> flux) {
        return null;
    }

    // TODO Op2:4 Remove duplicated news in sequences
    public Flux<News> removeDuplicatedNewsInSequences(Flux<News> flux) {
        return null;
    }

    // TODO Op2:5 Take first two news
    public Flux<News> takeTwo(Flux<News> flux) {
        return null;
    }

    // TODO Op2:6 Take last news
    public Flux<News> takeLast(Flux<News> flux) {
        return null;
    }

    // TODO Op2:7 take all  but first two news
    public Flux<News> skipTwo(Flux<News> flux) {
        return null;
    }

    // TODO Op2:8 Take news until sport news
    public Flux<News> takeUntilSport(Flux<News> flux) {
        return null;
    }


}
