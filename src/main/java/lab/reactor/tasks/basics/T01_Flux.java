package lab.reactor.tasks.basics;

import lab.news.domain.News;
import reactor.core.publisher.Flux;

import java.util.List;

public class T01_Flux {

    // TODO 1 create empty flux
    public Flux<News> fluxEmpty(){
        return Flux.empty();
    }

    // TODO 2 create flux from values
    public Flux<News> fluxFromValues(News news){
        return Flux.just(news);
    }

    // TODO 3 create flux from list
    public Flux<News> fluxFromList(List<News> newsList){
        return Flux.fromIterable(newsList);
    }

    // TODO 4 create flux emitting error
    public Flux<News> fluxEmittingError(){
        return Flux.error(new UnsupportedOperationException("error cause"));
    }

}
