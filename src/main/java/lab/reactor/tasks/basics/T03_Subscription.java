package lab.reactor.tasks.basics;

import lab.news.domain.News;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.BaseSubscriber;

public class T03_Subscription {

    private static final Logger log = LoggerFactory.getLogger(T03_Subscription.class);


    // TODO 4:1 Write a subscriber that requests 2 items
    // and cancels subscription when it receives all of them
    public Subscriber<News> twoItemsSubscriber() {

        return new BaseSubscriber<>() {

            private final int max_elements = 2;
            private int counter;

            @Override
            protected void hookOnSubscribe(Subscription subscription) {
                log.info("hook subscribe: {}", subscription);
                request(max_elements);
            }

            @Override
            protected void hookOnNext(News value) {
                counter++;
                log.info("hook next long: {}", value);
                if (counter==max_elements) {
                    //onComplete();
                    cancel();
                }
            }

            @Override
            protected void hookOnError(Throwable throwable) {
                log.error("hook error in stream", throwable);
            }

            @Override
            protected void hookOnComplete() {
                log.info("hook stream completed.");
            }

            @Override
            protected void hookOnCancel() {
                log.info("hook cancel.");
            }
        };
    }


//========================================================================================

    // TODO 4:2 Write a subscriber that requests item in batches of #{batchSize}
    // and cancels subscription if number of received items reaches #{maxAmount}
    public Subscriber<News> batchSubscriber(int batchSize, int maxAmount) {
        return new BaseSubscriber<>() {

            private int counter;

            @Override
            protected void hookOnSubscribe(Subscription subscription) {
                log.info("hook subscribe: {}", subscription);
                request(batchSize);
            }

            @Override
            protected void hookOnNext(News value) {
                log.info("hook next long: {}", value);
                counter++;

                if (counter==maxAmount) {
                    log.info("max.");
                    cancel();
                }
                if(counter%batchSize==0){
                    log.info("requesting another batch");
                    request(batchSize);
                }
            }

            @Override
            protected void hookOnError(Throwable throwable) {
                log.error("hook error in stream", throwable);
            }

            @Override
            protected void hookOnComplete() {
                log.info("hook stream completed.");
            }

            @Override
            protected void hookOnCancel() {
                log.info("hook cancel.");
            }
        };
    }
}
