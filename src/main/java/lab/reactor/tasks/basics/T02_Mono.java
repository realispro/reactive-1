package lab.reactor.tasks.basics;

import lab.news.domain.News;
import reactor.core.publisher.Mono;

public class T02_Mono {

    // TODO Return an empty Mono
    public Mono<News> emptyMono() {
        return Mono.empty();
    }

//========================================================================================

    // TODO Return a Mono that never emits any signal
    public Mono<News> monoWithNoSignal() {
        return Mono.never();
    }

//========================================================================================

    // TODO Return a Mono that contains value
    public Mono<News> valueMono() {
        return Mono.just(News.POLAND_WON_MUNDIAL);
    }

//========================================================================================

    // TODO Create a Mono that emits an IllegalStateException
    public Mono<News> errorMono() {
        return Mono.error(new IllegalArgumentException("test exception"));
    }
}
