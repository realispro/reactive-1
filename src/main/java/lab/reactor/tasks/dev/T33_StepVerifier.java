package lab.reactor.tasks.dev;

import lab.news.domain.Category;
import lab.news.domain.News;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.function.Supplier;

public class T33_StepVerifier {

    // TODO Dev3:1 check that the flux parameter emits POLITICS and SPORTS elements then completes
    public void expectPoliticsSportComplete(Flux<News> flux) {
        StepVerifier.create(flux)
                .expectNextMatches(news->news.getCategory()==Category.POLITICS)
                .expectNextMatches(news->news.getCategory()==Category.SPORT)
                .verifyComplete();
    }

    // TODO Dev3:2 check that the flux parameter emits POLITICS and SPORTS elements then a RuntimeException error.
    public void expectPoliticsSportError(Flux<News> flux) {
        StepVerifier.create(flux)
                .expectNextMatches(news->news.getCategory()==Category.POLITICS)
                .expectNextMatches(news->news.getCategory()==Category.SPORT)
                .expectError(RuntimeException.class).verify();
    }

    // TODO Dev3:3 Expect 3 elements then complete
    public void expect3Elements(Flux<News> flux) {
        StepVerifier.create(flux)
                .expectNextCount(3)
                .verifyComplete();
    }


    // TODO Dev3:4 Expect 3600 elements at intervals of 1 second, and verify quicker than 3600s
    // by manipulating virtual time thanks to StepVerifier#withVirtualTime
    public void expect3600Elements(Supplier<Flux<News>> supplier) {
        StepVerifier.Step<News> verifier = StepVerifier.withVirtualTime(supplier).expectSubscription();
        for (int i = 0; i < 3600; ++i) {
            verifier = verifier.expectNoEvent(Duration.ofSeconds(1)).expectNextCount(1);
        }
        verifier.verifyComplete();
    }

}
