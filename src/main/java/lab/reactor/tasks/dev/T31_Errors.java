package lab.reactor.tasks.dev;

import lab.news.domain.Category;
import lab.news.domain.News;
import reactor.core.Exceptions;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class T31_Errors {


    // TODO Dev1:1 Return a Mono<News> containing News.PANDEMIC_ENDS when an error occurs in the input Mono
    public Mono<News> pandemicEndsForErrorMono(Mono<News> mono) {
        return mono.onErrorReturn(News.PANDEMIC_ENDS);
    }


    // TODO Dev1:2 Return a Flux<News> containing News.PANDEMIC_END and News.INFLATION_DROP when an error occurs in the input Flux
    public Flux<News> positiveNewsForErrorFlux(Flux<News> flux) {
        return flux.onErrorResume(t -> Flux.just(News.PANDEMIC_ENDS, News.INFLATION_DROPPED));
    }


    // TODO Dev1:3 capitalizes each title using the #capitalizeUser method and emits encountered exceptions
    public Flux<News> capitalizeTitles(Flux<News> flux) {
        return flux.map(news -> {
            try {
                return capitalizeTitle(news);
            } catch (RestrictedCategoryException e) {
                throw Exceptions.propagate(e);
            }
        });
    }

    private News capitalizeTitle(News news) throws RestrictedCategoryException {
        if (news.getCategory()== Category.ENTERTAINMENT) {
            throw new RestrictedCategoryException();
        }
        return new News(news.getCategory(), news.getHeadline().toUpperCase(), news.getText());
    }

    protected final class RestrictedCategoryException extends Exception {
        private static final long serialVersionUID = 0L;
    }
}
